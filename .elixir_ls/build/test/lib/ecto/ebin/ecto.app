{application,ecto,
             [{applications,[kernel,stdlib,elixir,logger,crypto,decimal]},
              {description,"A toolkit for data mapping and language integrated query for Elixir"},
              {modules,[]},
              {registered,[]},
              {vsn,"3.0.3"},
              {mod,{'Elixir.Ecto.Application',[]}}]}.
