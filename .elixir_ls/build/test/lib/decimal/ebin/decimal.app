{application,decimal,
             [{applications,[kernel,stdlib,elixir]},
              {description,"Arbitrary precision decimal arithmetic."},
              {modules,[]},
              {registered,[]},
              {vsn,"1.6.0"}]}.
