{application,phoenix_ecto,
             [{applications,[kernel,stdlib,elixir,logger,ecto,plug]},
              {description,"Integration between Phoenix & Ecto"},
              {modules,[]},
              {registered,[]},
              {vsn,"4.0.0"},
              {mod,{'Elixir.Phoenix.Ecto',[]}},
              {env,[{exclude_ecto_exceptions_from_plug,[]}]}]}.
