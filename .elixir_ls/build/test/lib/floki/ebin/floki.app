{application,floki,
             [{applications,[kernel,stdlib,elixir,logger,mochiweb]},
              {description,"Floki is a simple HTML parser that enables search for nodes using CSS selectors."},
              {modules,[]},
              {registered,[]},
              {vsn,"0.17.2"}]}.
