{application,telemetry,
             [{applications,[kernel,stdlib,elixir,logger]},
              {description,"Dynamic dispatching library for metrics and instrumentations.\n"},
              {modules,[]},
              {registered,[]},
              {vsn,"0.2.0"},
              {mod,{'Elixir.Telemetry.Application',[]}}]}.
