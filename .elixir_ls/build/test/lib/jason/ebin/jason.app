{application,jason,
             [{applications,[kernel,stdlib,elixir]},
              {description,"A blazing fast JSON parser and generator in pure Elixir.\n"},
              {modules,[]},
              {registered,[]},
              {vsn,"1.1.2"}]}.
