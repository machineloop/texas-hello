{application,phoenix_pubsub,
             [{applications,[kernel,stdlib,elixir,logger,crypto]},
              {description,"Distributed PubSub and Presence platform\n"},
              {modules,[]},
              {registered,[]},
              {vsn,"1.1.1"},
              {mod,{'Elixir.Phoenix.PubSub.Supervisor',[]}}]}.
