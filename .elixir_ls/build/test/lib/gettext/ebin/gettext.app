{application,gettext,
             [{applications,[kernel,stdlib,elixir,logger]},
              {description,"Internationalization and localization through gettext"},
              {modules,[]},
              {registered,[]},
              {vsn,"0.16.1"},
              {env,[{default_locale,<<"en">>}]},
              {mod,{'Elixir.Gettext.Application',[]}}]}.
