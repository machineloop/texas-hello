defmodule HelloWeb.PageView do
  use HelloWeb, :view
  def data(conn) do
    %{ example_list: example_list(conn) }
  end

  def example_list(_conn) do
    ~s|
      <div class="some-class">
        #{example_text_node()}
      </div>
    |
    # ultimately texas needs AST, not html string
    # so we parse it out here for now
    # (future versions will clean this up so you can just worry about html strings)
    |> Floki.parse()
  end

  defp example_text_node, do: "<div> see it's overwritten! </div>"
end
