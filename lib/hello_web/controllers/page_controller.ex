defmodule HelloWeb.PageController do
  use HelloWeb, :controller

  def index(conn, _params) do
    texas_render conn, "index.html", [texas: HelloWeb.PageView.data(conn)]
  end

end
